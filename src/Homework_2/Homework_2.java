package Homework_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Homework_2 {
    private static BufferedReader reader;

    public static void main(String[] args) throws IOException {
        BufferedReader  reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите номер задания:");
        System.out.println("1,2,3,4,5,6,7,8,0 - выход");
        String check = reader.readLine();
        while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
            System.out.println("Не верное значение");
            check = reader.readLine();
        }
        int o = Integer.parseInt(check);
        while (o!=0){
            switch (o){
                case 1:
                    System.out.println("Введите 3 числа");
                    int n = 0;
                    int m = 0;
                    int v = 0;
                    String line1 = reader.readLine();
                    while (line1.isEmpty()){
                        System.out.println("Не дури");
                        line1 = reader.readLine();
                    }
                    String line2 = reader.readLine();
                    while (line2.isEmpty()){
                        System.out.println("Не дури");
                        line2 = reader.readLine();
                    }
                    String line3 = reader.readLine();
                    while (line3.isEmpty()){
                        System.out.println("Не дури");
                        line3 = reader.readLine();
                    }
                    n = Integer.parseInt(line1);
                    m = Integer.parseInt(line2);
                    v = Integer.parseInt(line3);
                    System.out.print("Ответ - ");
                    System.out.println( MinOfThree(n,m,v));
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 2:
                    System.out.println("Введите число для нахождения факториала");
                    int s = Integer.parseInt(reader.readLine());
                    System.out.println(Factorial1(s));
                    System.out.println(Factorial2(s));
                    System.out.println(Factorial3(s));
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 3:
                    DimensionalArray();
                    System.out.println("");
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 4:
                    DimensionalArray5();
                    System.out.println("");
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 5:
                    System.out.println("Введите числа и операцию");
                    int q = 0;
                    int w = 0;
                    String lin1 = reader.readLine();
                    while (lin1.isEmpty()){
                        System.out.println("Не дури");
                        lin1 = reader.readLine();
                    }
                    String lin2 = reader.readLine();
                    while (lin2.isEmpty()){
                        System.out.println("Не дури");
                        lin2 = reader.readLine();
                    }
                    q = Integer.parseInt(lin1);
                    w = Integer.parseInt(lin2);
                    String p = reader.readLine();
                    while (!p.equals("+") && !p.equals("-") && !p.equals("*") && !p.equals("/")){
                        System.out.println("Ага, опять пытался, давай вводи операцию!");
                        p = reader.readLine();
                    }
                    Calculator(q,w,p);
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 6:
                    MinMax();
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 7:
                    Matrix();
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
                case 8:
                    Week();
                    System.out.println("Какое задание теперь?");
                    System.out.println("1,2,3,4,5,6,7,8,0 - выход");
                    check = reader.readLine();
                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
                        System.out.println("Не верное значение");
                        check = reader.readLine();
                    }
                    o = Integer.parseInt(check);
                    break;
//                default:
//                    System.out.println("Неверное значение, попробуйте снова");
//                    check = reader.readLine();
//                    while (!check.equals("0") && !check.equals("1") && !check.equals("2") && !check.equals("3") && !check.equals("4") && !check.equals("5") && !check.equals("6") && !check.equals("7") && !check.equals("8")){
//                        System.out.println("Не верное значение");
//                        check = reader.readLine();
//                    }
//                    o = Integer.parseInt(check);
//                    break;
            }
        }
    }

    public static int MinOfThree(int a, int b, int c) {
            if ((a < b) && (a < c)) {
                return a;
            }
            if ((b < a) && (b < c)) {
                return b;
            }
            if ((c < a) && (c < b)) {
                return c;
            }
        return 0;
    }

    public static int Factorial1(int a){
        int x =1;
        while (a>0){
            x=x*a;
            a--;
        }
        return x;
    }

    public static int Factorial2(int a){
        int x= 1;
        do {
            x=x*a;
            a--;
        } while (a>0);
            return x;
    }

    public static int Factorial3(int a){
        int x = 1;
        for (int i = 1; i<=a; i++){
            x=x*i;
        }
        return x;
    }

    public static void DimensionalArray(){
    int [] array = {2,5,3,6,9,2,4,8,5,2,5,8,3,5,6,2,5,8,1,4};
        System.out.println("Данный массив");
    for (int x=0; x<20; x++){
        System.out.print(array[x]);
    }
        System.out.println("");
        System.out.println("Первые 10 чисел:");
    for (int i=0; i<10; i++)
        System.out.print(array[i] + " ");
    }

    public static void DimensionalArray5() {
        int [] array = {2,5,3,6,9,5,4,8,5,2,5,8,3,5,6,2,5,8,1,4};
        System.out.println("");
        System.out.println("Данный массив");
        for (int x=0; x<20; x++){
            System.out.print(array[x]);
        }
        System.out.println("");
        System.out.println("Массив без 5:");
        for (int i=0; i<array.length; i++) {
            if (array[i]!=5)
            System.out.print(array[i] + " ");
        }
    }

    public static void Calculator(int a, int b, String c) throws IOException {
        switch (c){
            case "-":
                System.out.println("= " + (a-b));
                break;
            case "+":
                System.out.println("= " + (a+b));
                break;
            case "*":
                System.out.println("= " + (a*b));
                break;
            case "/":
                System.out.println("= " + (float)a/b);
                break;
            default:
                System.out.println("Не верный символ");
                break;
        }
    }

    public static void MinMax(){
        int max=0;
        int min=10;
        int [] array = {1,5,2,6,2,6,4,2,3,8};
        int [] [] matrix = {{1,6,3},{5,2,7},{6,2,5}};
        System.out.println("Данная одномерная матрица");
        for (int i=0; i<10; i++){
            System.out.print(array[i]);
            }
        System.out.println("");
        System.out.println("Данная двумерная матрица");
        for (int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
        for (int i=0; i<10; i++){
          if (array[i]>max){
              max=array[i];
          }
          if (array[i]<min){
                min=array[i];
          }
        }
        System.out.println("Min и max одномерной матрицы");
        System.out.println("min =" + min);
        System.out.println("max =" + max);

        max=0;
        min=10;
        for (int i=0;i<3;i++)
            for (int j=0;j<3;j++){
            if (matrix[i][j]>max)
                max=matrix[i][j];
            if (matrix[i][j]<min)
                min=matrix[i][j];
            }
        System.out.println("Min и max двумерной матрицы");
        System.out.println("min =" + min);
        System.out.println("max =" + max);
    }

    public static void Matrix(){
        int[][] matrix = {{6,2,4,2,3},{3,5,2,4,8},{1,5,3,6,2},{1,6,3,5,1},{1,6,2,4,7}};
        int summ=0;
        System.out.println("Данная матрица");
        for (int i = 0; i<5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(matrix[i][j] + " ");
                if (j < i) {
                    summ = summ + matrix[i][j];
                }
            }
            System.out.println("");
        }
        System.out.println("Сумма всех элементов, ниже главной диагонали = " + summ);
    }

    public static void Week(){
        int x=0;
        int[] month = new int[31];
        System.out.println("числа всех пятниц в этом месяце:");
        for (int i =0; i<31;i++){
            month[i]=i+1;
        }
        for (int i =0; i<31;i++){
            if (i==(4+7*x)){
                System.out.print(month[i]+" ");
                x++;
            }
        }
        int d = 31-7*x;
        System.out.println("");
        switch (d){
            case 1:
                System.out.println("31 число - Понедельник");
                break;
            case 2:
                System.out.println("31 число - Вторник");
                break;
            case 3:
                System.out.println("31 число - Среда");
                break;
            case 4:
                System.out.println("31 число - Четверг");
                break;
            case 5:
                System.out.println("31 число - Пятница");
                break;
            case 6:
                System.out.println("31 число - Суббота");
                break;
            case 7:
                System.out.println("31 число - Воскресенье");
                break;
        }
    }
}